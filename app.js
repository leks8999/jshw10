const tabs = document.querySelector('.tabs')
const tabsBtn = document.querySelectorAll('.tabs-title')
const tabsItems = document.querySelectorAll('.tabs-item')

tabs.addEventListener('click', (e) => {
    tabsBtn.forEach(item => {
        if (e.target === item) {
            item.classList.add('active')
        } else { item.classList.remove('active') }
    })
    tabsItems.forEach(item => {
        if (item.dataset.tab === e.target.dataset.tab) {
            item.classList.add('active')
        } else { item.classList.remove('active') }
    })
})




